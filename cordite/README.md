# Cordite

[Cordite](https://cordite.foundation/) provides decentralised economics and governance services. Cordite is regulatory friendly, enterprise ready and finance grade. Cordite is an open source CorDapp, built on [Corda](https://corda.net).

## TL;DR;

```bash
$ helm repo add cordite-charts https://cordite.gitlab.io/cloud-templates
$ helm install --name my-release \
    --set cordite.myLegalName="O=<ChangeThis>, L=London, C=GB",\
          cordite.myPublicAddress="cordite.example.com",\
          cordite.myEmailAddress="noreply@cordite.foundation",\
          cordite.myNetwork="CORDITE TEST" \
    cordite-charts/cordite 
```

## Introduction

This chart bootstraps a [Cordite](http://gitlab.com/cordite/cordite) node deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

We have found [Helm](https://helm.sh/) is the best way to find, share, and use software built for [Kubernetes](https://kubernetes.io/). Kubernetes (K8s) is an open-source system for automating deployment, scaling, and management of containerized applications. You will find Kubernetes is available on all good Cloud providers.  

Lovers of homebrew can use `brew install kubernetes-helm` followed by `helm init`. For everyone else head over to [helm install instructions](https://github.com/helm/helm#install)  

Don't have a Kubernetes cluster? Every new Google Cloud Platform (GCP) account receives [$300 in credit upon sign up](https://console.cloud.google.com/freetrial). Once signed up you can [create a cluster](https://console.cloud.google.com/kubernetes)

Want to test locally? You can use Docker Desktop (18.06+) for [MacOS](https://docs.docker.com/docker-for-mac/#kubernetes) or [Windows](https://docs.docker.com/docker-for-windows/#kubernetes), which comes with Kubernetes embedded.  

## Prerequisites
- Kubernetes 1.8+
- PV provisioner support in the underlying infrastructure

## Installing the Chart
To install the chart with the release name `my-release`:

```bash
$ helm repo add cordite-charts https://cordite.gitlab.io/cloud-templates
$ helm install --name my-release \
    --set cordite.myLegalName="O=Cordite-helm-template, L=London, C=GB",\
          cordite.myPublicAddress="cordite.example.com",\
          cordite.myEmailAddress="noreply@cordite.foundation",\
          cordite.myNetwork="CORDITE TEST" \
    cordite-charts/cordite 
```

The command deploys Cordite on the Kubernetes cluster in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart
To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration

The following table lists the configurable parameters of the Cordite chart and their default values.

| Parameter                            | Description                                      | Default                                                 |
| ------------------------------------ | ------------------------------------------------ | ------------------------------------------------------- |
| `myLegalName` | The [Corda Network X500 name](https://corda.network/participation/index.html#) to use for this node | `O=Cordite-XXX, OU=Cordite, L=London, C=GB` |
| `myPublicAddress`   | The public FQDN to advertise the node on | `localhost` |
| `myEmailAddress` | The email address the network operator can reach you | `noreply@cordite.foundation` |
| `image.repository`                   | Cordite Image name                              | `cordite/cordite`                                      |
| `image.tag`                          | Cordite Image tag                               | `test`                                             |
| `image.pullPolicy`                   | Image pull policy                                | `Always`                                 |
| `persistence.enabled`                | Use a PVC to persist data                        | `true`                                                 |
| `persistence.storageClass`           | Storage class of backing PVC                     | `nil` (uses alpha storage class annotation)             |
| `persistence.accessMode`             | Use volume as ReadOnly or ReadWrite              | `ReadWriteOnce`                                         |
| `persistence.size`                   | Size of data volume                              | `2Gi`                                                   |
| `service.type`                       | Kubernetes Service type                          | `LoadBalancer`                                             |
| `service.p2pPort`                    | The public FQDN to advertise the node on         | `10200`                                                  |
| `service.braidPort`                  | [Braid](https://gitlab.com/cordite/braid) port   | `443`                                                 |
| `service.nodePort`                   | Node port override, if serviceType NodePort      | _random available between 30000-32767_                  |
| `securityContext.enabled`            | Enable security context                          | `true`                                                  |
| `securityContext.fsGroup`            | Group ID for the container                       | `1000`                                                  |
| `securityContext.runAsUser`          | User ID for the container                        | `1000`                                                  |
| `resources`                          | resource needs and limits to apply to the pod    | {}                                                      |
| `nodeSelector`                       | Node labels for pod assignment                   | {}                                                      |
| `affinity`                           | Affinity settings for pod assignment             | {}                                                      |
| `tolerations`                        | Toleration labels for pod assignment             | []                                                      |
| `postgresql.enabled`                 | Postgres DB to be created                        | `true`                                                  |
| `postgresql.postgresUsername`        | Postgres DB username | `sa` | 
| `postgresql.postgresPassword`        | Postgres DB password | `dbpass` |
| `postgresql.persistence.enabled`                | Use a PVC to persist data                        | `true`                                                 |
| `postgresql.persistence.storageClass`           | Storage class of backing PVC                     | `nil` (uses alpha storage class annotation)             |
| `postgresql.persistence.accessMode`             | Use volume as ReadOnly or ReadWrite              | `ReadWriteOnce`                                         |
| `postgresql.persistence.size`                   | Size of data volume                              | `2Gi`                                                   |
| `postgresql.resources`                          | resource needs and limits to apply to the pod    | {}                                                      |

The above parameters map to the env variables defined in [Cordite](http://gitlab.com/cordite/cordite). For more information please refer to the [Cordite](http://gitlab.com/cordite/cordite) documentation.

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```bash
$ helm install --name my-release \
    --set cordite.myLegalName="O=Cordite-helm-template, L=London, C=GB",\
          cordite.myPublicAddress="cordite.example.com",\
          cordite.myEmailAddress="noreply@cordite.foundation",\
          cordite.myNetwork="CORDITE TEST" \
    cordite-charts/cordite 
```

## Production

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. To operate this chart in a production environment, we recommend you use a values.yaml under SCM.

```bash
$ helm install --name my-release -f values.yaml cordite-charts/cordite 
```

> **Tip**: You can use the default [values.yaml](values.yaml)


## Persistence
The chart mounts a [Persistent Volume](http://kubernetes.io/docs/user-guide/persistent-volumes/) at `/opt/corda/persistence` and `opt/corda/certificates`. By default, the volume is created using dynamic volume provisioning. This chart uses [Postgres](https://www.postgresql.org/) as a DB rather than H2. The Postgres instance is created as a sub-chart and has its own Persistent Volume.

### Existing PersistentVolumeClaims
Raise an issue if you want this

## Upgrading
Backwards compatibility is not guaranteed until further testing is complete.

Happy Helming
