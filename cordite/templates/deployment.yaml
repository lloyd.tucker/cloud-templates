apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: {{ include "cordite.fullname" . }}
  labels:
    app.kubernetes.io/name: {{ include "cordite.name" . }}
    helm.sh/chart: {{ include "cordite.chart" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- if .Values.annotations }}
  annotations:
{{ toYaml .Values.annotations | indent 4 }}
{{- end }}
spec:
  replicas: {{ default "1"  .Values.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "cordite.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "cordite.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
      containers:
        - name: {{ .Chart.Name }}
          {{- $repo := default "cordite/cordite" .Values.image.repository }}
          {{- $tag := default "latest" .Values.image.tag }}
          image: "{{ $repo }}:{{ $tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy | default "Always" }}
          env:
            - name: MY_LEGAL_NAME
              value: {{ required "myLegalName value is required!" .Values.myLegalName | quote }}
            - name: MY_PUBLIC_ADDRESS
              value: {{ required "myPublicAddress value is required!" .Values.myPublicAddress | quote }}
            - name: MY_EMAIL_ADDRESS
              value: {{ required "myEmailAddress value is required!" .Values.myEmailAddress | quote }}
            {{- if (eq .Values.myNetwork "CORDITE TEST") }}
            - name: NETWORKMAP_URL
              value: "https://nms-test.cordite.foundation"
            - name: DOORMAN_URL
              value: "https://nms-test.cordite.foundation"
            - name: CORDITE_DEV_MODE
              value: "true"
            {{- end }}
            {{- if (eq .Values.postgresql.enabled true) }} # if false will default to using H2; can be overriden by setting CORDITE_DB_URL
            - name: CORDITE_DB_URL
              value: "jdbc:postgresql://{{ .Release.Name }}-postgresql:5432/cordite"
            - name: CORDITE_DB_DRIVER
              value: "org.postgresql.ds.PGSimpleDataSource"
            - name: CORDITE_DB_USER
              value: {{ required "postgresqlUsername value is required of postgressql enabled!" .Values.postgresql.postgresqlUsername | quote }}
            - name: CORDITE_DB_PASS
              value: {{ required "postgresqlPassword value is required of postgressql enabled" .Values.postgresql.postgresqlPassword | quote }}
            {{- end }}
            - name: CORDITE_CACHE_NODEINFO
              value: "true"
            - name: CORDITE_BRAID_PORT
              value: "8443"
            - name: MY_P2P_PORT
              value: "10200"
          ports:
            - name: braid
              containerPort: 8443  
              protocol: TCP
            - name: p2p
              containerPort: 10200
              protocol: TCP
          readinessProbe:
            httpGet:
              path: /
              port: braid
            initialDelaySeconds: 300
            periodSeconds: 10
          livenessProbe:
            httpGet:
              path: /
              port: braid
            initialDelaySeconds: 300
            periodSeconds: 10
          volumeMounts:
            - name: persistence
              mountPath: "/opt/corda/persistence"
              subPath: persistence
            - name: certificates
              mountPath: "/opt/corda/certificates"
              subPath: certificates
          resources:
{{ toYaml .Values.resources | indent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      securityContext:
        runAsUser: {{ .Values.securityContext.runAsUser }}
        fsGroup: {{ .Values.securityContext.fsGroup }}
      volumes:
        - name: certificates
        {{- if (eq .Values.persistence.enabled true) }}
          persistentVolumeClaim:
            claimName: {{ template "cordite.fullname" . }}
        {{- else}}
          emptyDir: {}
        {{- end }}
        - name: persistence
        {{- if (eq .Values.persistence.enabled true) }}
          persistentVolumeClaim:
            claimName: {{ template "cordite.fullname" . }}
        {{- else}}
          emptyDir: {}
        {{- end }}
