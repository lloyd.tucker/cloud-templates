#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
# Default values for helm-cordite-node.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

# https://gitlab.com/cordite/network-map-service#command-line-parameters
# Missing environment variables will be defaulted 

# Pod
nms:
  replicaCount: 1

  # Image ref: http://kubernetes.io/docs/user-guide/images/
  image:
    repository: cordite/network-map 
    tag: latest
    pullPolicy: Always
  env:
    NMS_CACHE_TIMEOUT: "6S" 
    NMS_CERTMAN: "false" 
    NMS_CERTMAN_PKIX: "false" 
    NMS_CERTMAN_STRICT_EV: "false"
    NMS_CERTMAN_TRUSTSTORE_PASSWORD: "pass"
    NMS_DOORMAN: "false"
    NMS_NETWORKMAP_DELAY: "1S"
    NMS_PARAM_UPDATE_DELAY: "10S"
    NMS_ROOT_CA_NAME: "CN=NMS-HELM, OU=Cordite Foundation Network, O=Cordite Foundation, L=London, ST=London, C=GB"
    NMS_WEB_ROOT: "/"
  
  # Secrets ref: https://kubernetes.io/docs/concepts/configuration/secret/
  secrets:
    #certman_truststore: "cacerts"
    #tls_cert: "examples.crt"
    #tls_key: "examples.key"
    dummy: null

  # Service ref: https://kubernetes.io/docs/concepts/services-networking/service/
  service:
    type: LoadBalancer
    # loadBalancerIP: 0.0.0.0
    port: 8080

  # Security Context ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
  securityContext:   
    enabled: true
    runAsUser: 1000
    fsGroup: 1000 

  # Resources ref: http://kubernetes.io/docs/user-guide/compute-resources/
  resources: {}
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    # limits:
    #  cpu: 100m
    #  memory: 128Mi
    # requests:
    #  cpu: 100m
    #  memory: 128Mi

  # Node selector ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#nodeselector
  nodeSelector: {}

  # Tolerations ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  tolerations: []

  # Affinity ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
  affinity: {}


# Database ref: https://github.com/kubernetes/charts/blob/master/stable/mongodb/README.md
mongodb:
  enabled: true
  persistence:
    enabled: true
    size: 4Gi
  resources: {}
  #  requests:
  #    memory: "2Gi"
  #    cpu: "100m"
  #  limits:
  #    memory: "2Gi"
  #    cpu: "250m"
  ## Make sure the --wiredTigerCacheSizeGB is no more than half the memory limit!
  ## This is critical to protect against OOMKill by Kubernetes!
  mongodbExtraFlags:
  - "--wiredTigerCacheSizeGB=1"
  usePassword: true
  mongodbUsername: nms
  mongodbPassword: password
  mongodbDatabase: nms