# Cordite Network Map Service : Helm Chart

## Overview

Cordite Network Map Service (Cordite NMS) is an implementation of the [Corda Network Map Service protocol](https://docs.corda.net/network-map.html). It has the following features:  

1. Meets the requirements both documented and otherwise
2. A nominal implementation of the Doorman protocol
3. A new protocol Certman for registration with client-provided certificates
4. Completely stateless - capable of running in load-balanced clusters
5. Efficient use of I/O to serve 5000+ concurrent read requests per second from a modest server
6. Transparent filesystem design to simplify maintenance, backup, and testing

For more information, see the [Cordite Network Map Service repo](https://gitlab.com/cordite/network-map-service)  

## Installation

### Quick install with Google Cloud Marketplace

Get up and running with a few clicks! Install this Cordite Network Map Service (Cordite NMS) app to a Google Kubernetes Engine cluster using Google Cloud Marketplace. Follow the on-screen
instructions: *TODO: link to solution details page*

### Command line instructions

Follow these instructions to install Cordite NMS from the command line.

#### Prerequisites
(We have found [Helm](https://helm.sh/) is the best way to find, share, and use software built for [Kubernetes](https://kubernetes.io/). Kubernetes (K8s) is an open-source system for automating deployment, scaling, and management of containerized applications. You will find Kubernetes is available on all good Cloud providers.  

Lovers of homebrew can use `brew install kubernetes-helm` followed by `helm init`. For everyone else head over to [helm install instructions](https://github.com/helm/helm#install)  

Don't have a Kubernetes cluster? Every new Google Cloud Platform (GCP) account receives [ in credit upon sign up](https://console.cloud.google.com/freetrial). Once signed up you can [create a cluster](https://console.cloud.google.com/kubernetes)

Want to test locally? You can use Docker Desktop (18.06+) for [MacOS](https://docs.docker.com/docker-for-mac/#kubernetes) or [Windows](https://docs.docker.com/docker-for-windows/#kubernetes), which comes with Kubernetes embedded.

#### Install

The easiest way to install Cordite NMS is using `helm`:
```
helm repo add cordite-charts https://cordite.gitlab.io/cloud-templates
helm install cordite-charts/cordite-nms
```

alternatively using kubectl:  
```
helm repo add cordite-charts https://cordite.gitlab.io/cloud-templates
helm template cordite-charts/cordite-nms > expanded.yaml
kubectl apply -f expanded.yaml
```

### Backups  
*TODO: instructions for backups*

### Upgrades  
*TODO: instructions for upgrades*

### Common issues

The following fixed `Error: no available release name found`
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```

Example key and cert can be created using:
```openssl req -newkey rsa:2048 -nodes -keyout example_key.pem -x509 -days 365 -out example_cert.pem```

*TODO: 
   1. test TLS
   2. test truststore
   3. remove hard coding of mongodb connection string and use values
   4. remove dummy: null