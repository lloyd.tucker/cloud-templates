
# Other

This directory contains other templates for deploying a Cordite node on popular cloud provider platforms.
* [ ] Docker CE swarm
* [x] [Azure Kubernetes Service](#azure-kubernetes-service)
* [x] [Google Cloud Platform](#google-cloud-platform)
* [ ] Amazon (ECS)
* [ ] RedHat OpenShift
* [x] [Native Kubernetes](#native-kubernetes)
* [x] [Native Docker Compose](#native-docker-compose)
* [ ] IBM BlueMix
* [ ] Terraform
* [x] [Helm](#helm)

## Native Docker Compose
Modify the legal entity information as required.
```
CORDITE_LEGAL_NAME=O=MyOrg, OU=MyOrgUnit, L=London, C=GB
CORDITE_P2P_ADDRESS=localhost:10002
docker-compose up -f ./docker-compose/docker-compose.yml
```

## Native Kubernetes
Kubernetes deployments assume the namespace cordite-node.
To deploy to a custome namespace edit the namespace value in the yaml files. Modify the variable 
CORDITE_P2P_ADDRESS=localhost:10002 to the hostname or IP address of your node.
```
git clone https://gitlab.com/cordite/cloud-templates.git
cd cloud-templates
kubectl create -f ./kubernetes/

```

## Helm
Helm templates simplfy deloyments across different platforms. Modify the vaules for your target platform.
``` 
# Reference the namespace excplicitely
helm upgrade --install cordite-node ./helm --namespace <namespace>
```

## Google Cloud Platform 
The GCP example assumes you have access to a K8s cluster on GCP.
```
git clone https://gitlab.com/cordite/cloud-templates.git
cd cloud-templates/gcp
./deploy.sh
```

## Azure Kubernetes Service
!!! An exmaple ingress deployment is provided for testing  purposes only and is not yet functional !!!
Modify the files for your environment before executing these commands. Check the ingress controller 
reference files for the inclusion if the tcp and udp configuration maps.
```
git clone https://gitlab.com/cordite/cloud-templates.git
cd cloud-templates
kubectl create namespace cordite-node
kubectl create -f ./aks
kubectl create configmap tcp-services --from-file=./aks/tcp-services-configmap

# To remove the configuration.

kubectl delete namespace cordite-node
kubectl delete configmap tcp-services

```
You can find information about network map server deployment in the respective folders

| Folder  | Features |
| ------------- | ------------- |
| nms  | http version of nms. It has an option to specify static ip address for the load balancer service  |
| nmstls-selfsigned-cert  | https version of nms with self signed certs  |
| nmstls-valid-cert  | https version of nms with your domain specific certs  |

# Testing token issuance and transfer between accounts.

## Prerequisites
For these examples, we will be using the Command Line Interface. This can be downloaded like so: `npm install -g cordite-cli`

You will need two Cordite nodes that are connected to a network-map-service. We recommend that you have two command line windows open, each connected to a different Cordite node.

## The following commands create an account on the first node. Then we create and issue a token.

MyOrg01 Node
```
notary = notaries.corditeBootstrapNotary.name
ledger.createAccount('account01', notary)
ledger.listAccounts()
ledger.createTokenType('XTS', '2', notary)
ledger.issueToken('account01', '100', 'XTS', 'issuance', notary)
ledger.balanceForAccount('account01')
```

## Next we create an account on the second node.

MyOrg02 Node
``` 
notary = notaries.corditeBootstrapNotary.name
ledger.createAccount('account02', notary)
ledger.listAccounts()
```

## We then initiate a balance transfer between the accounts.

MyOrg01 Node
``` 
ledger.transferToken('10', 'XTS:2:OU=MyOrg01, O=MyOrgUnit01, L=London, C=GB', 'account01', 'account02@OU=MyOrg02, O=MyOrgUnit02, L=London, C=GB', 'transfering tokens', notary)
```

## Finally we confirm the transfer by checking the balance on the accounts.

MyOrg02 Node
```
ledger.balanceForAccount('account02')
```

MyOrg01 Node
```
ledger.balanceForAccount('account01')
```

