cat ../yml/pvc.yml \
   | sed s/NAME_PLACEHOLDER/${RESOURCE_NAME}/ \
   | kubectl -n ${NAMESPACE} create -f -
