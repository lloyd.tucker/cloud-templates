cat ../yml/nms-deployment.yml \
    | sed s/VERSION_PLACEHOLDER/${VERSION}/ \
    | sed s/NAME_PLACEHOLDER/${RESOURCE_NAME}/ \
    | kubectl -n ${RESOURCE_NAME} create -f -
