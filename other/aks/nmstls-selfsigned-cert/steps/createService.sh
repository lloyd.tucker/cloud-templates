
CLIENT_ID=$(az aks show --resource-group $RESOURCE_GROUP --name $CLUSTER_NAME --query 'servicePrincipalProfile.clientId' | tr -d \")
echo "ClientId of cluster is: ${cyan}${CLIENT_ID}${reset}"

SCOPE=$(az group show --resource-group $RESOURCE_GROUP --query 'id' -o tsv)
echo "Scope of resource group is: ${cyan}${SCOPE}${reset}"
az role assignment create --assignee $CLIENT_ID --role "Network Contributor" --scope $SCOPE

IP_ADDRESS=$(az network public-ip show -g $RESOURCE_GROUP -n $NAMESPACE --query 'ipAddress' -o tsv)
echo "External ip address for service is: ${cyan}${IP_ADDRESS}${reset}"

cat ../yml/nms-service.yml \
  | sed s/NAME_PLACEHOLDER/${NAMESPACE}/ \
  | sed s/IP_PLACEHOLDER/${IP_ADDRESS}/ \
  | sed s/RG_PLACEHOLDER/${RESOURCE_GROUP}/ \
  | kubectl -n ${NAMESPACE} create -f -

