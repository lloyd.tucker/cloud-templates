cat ../yml/nms-deployment.yml \
    | sed s/VERSION_PLACEHOLDER/${VERSION}/ \
    | sed s/NAME_PLACEHOLDER/${NAMESPACE}/ \
    | kubectl -n ${NAMESPACE} create -f -
